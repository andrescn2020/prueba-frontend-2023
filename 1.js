// Ejercicio 1
// Dada una matriz de N elementos en la que todos los elementos son iguales excepto uno,
// crea una función findUniq que retorne el elemento único.

function findUniq(array) {
    
    let unique;

    const map = new Map();

    for (let i = 0; i < array.length; i++) {
      if (map.has(array[i])) {
        map.set(array[i], map.get(array[i]) + 1);
      } else {
        map.set(array[i], 1);
      }
    }

    map.forEach((value, key) => {
      if (value === 1) {
        unique = key;
      }
    });
    
    return unique;

};


/**
 * TEST Ejercicio 1
 */
findUniq(['12', 10, '12', 11, 1, 11, 10, '12']); // 1
findUniq(['Capitán América', 'Hulk', 'Deadpool', 'Capitán América', 'Hulk', 'Wonder Woman', 'Deadpool', 'Iron Man', 'Iron Man']); // 'Wonder Woman'
